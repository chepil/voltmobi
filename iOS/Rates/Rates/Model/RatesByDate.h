//
//  RatesByDate.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RatesByDate : NSObject
@property (nonatomic, strong) NSString *dateOfRequest;
@property (atomic, strong) NSArray *ratesArray;

-(NSString*)description;
@end
