//
//  UILabel+ratestyle.h
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FontSettings.h"

@interface UILabel (ratestyle)

- (void) setRatesStyle:(RatesFont)font
                  size:(CGFloat)fontSize
        hexColorString:(NSString*)hexColorString
               spacing:(CGFloat)spacing
            lineHeight:(CGFloat)lineHeight
      opacityInPercent:(unsigned int)opacityInPercent
      paragraphSpacing:(CGFloat)paragraphSpacing
                  text:(NSString*)text;


@end
