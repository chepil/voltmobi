//
//  Singleton.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "Singleton.h"
#import "JsonDataParser.h"

#define apiUrl @"https://api.fixer.io"




@implementation Singleton

dispatch_semaphore_t dataReloadSemaphore;

static Singleton* sSingleton = nil;

+(id)sharedInstance {
    static dispatch_once_t predicate;
    dispatch_once( &predicate, ^{
        sSingleton = [ [ self alloc ] init ];
        [sSingleton firstInit];
    } );
    return sSingleton;
}

-(void)firstInit {
    
    _lastSelectedIndex = 0;
    dataReloadSemaphore = dispatch_semaphore_create(1);
    _netLoader = [NetLoader new];
    _netLoader.delegate = self;
/*
     //так как в ТЗ говориться о том, что нет необходимости хранить контент в БД,
     //тупо делаем запросы в сеть за сегодня и вчера, чтобы получить котировки за 2 дня.
     //делаем это каждый раз, при открытии приложения, так как в ТЗ не сказано о варианте хранения данных.
     //таким образом, делается 6 сетевых запросов - 3 за сегодня, 3 за вчера
*/
    [self reloadData];
    
}

-(void)forceUpdate {
    [_netLoader setNoInternetConnection:FALSE];
    [self reloadData];
}

-(void)reloadData {
    
    if ([_netLoader noInternetConnection])
        return;
    
    if (_mainViewController) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            [[_mainViewController actNetworkWaiting] startAnimating];
        });
    }
    //первичный запрос в сеть, для latest
    [self latestRequest];
}

-(long)incReceiveDataCounter {
    _receiveDataCounter++;
    if (_receiveDataCounter >= 12) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            [[NSNotificationCenter defaultCenter] postNotificationName:firstObjectRecievedNotification
                                                                object:nil];
        });
    }
    
    return _receiveDataCounter;
}

-(void)latestRequest {
    if ([_netLoader noInternetConnection])
        return;
    
    _receiveDataCounter = 0;
    if (!_latestDateInString)
        _latestDateInString = @"";
    _needLatestDateUpdate = TRUE;
    //http://api.fixer.io/latest?base=RUB&symbols=USD,EUR
    NSString *shablon = @"%@/latest?base=%@&symbols=%@,%@";
    
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, @"USD",@"RUB",@"EUR"]];
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, @"RUB",@"USD",@"EUR"]];
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, @"EUR",@"USD",@"RUB"]];
}

#pragma mark -
#pragma mark NetLoader Delegate

-(void)didLoadJson:(_Nullable id)jsonData {
#ifdef DEBUG
    //NSLog(@"loaded data: %@", jsonData);
#endif
    [[JsonDataParser new] loadData:jsonData];
}

-(void)didCompleteWithError:(NSError* _Nullable)error {
#ifdef DEBUG
    NSLog(@"didCompleteWithError: %@", error);
#endif
}

#pragma mark -
#pragma mark Save Results to Database
-(void)writeInfoByDate:(NSString* _Nonnull)date andRateNamesArray:(RateObject* _Nonnull)rateObject {
    __weak __typeof__(self) weakSelf = self;
    dispatch_semaphore_wait(dataReloadSemaphore, DISPATCH_TIME_FOREVER);
    dispatch_sync(dispatch_get_main_queue(), ^{
        __typeof__(self) strongSelf = weakSelf;
        //Save Data to History Array
        
        //1) check if _ratesHistoryArray exists
        if (!_ratesHistoryArray) {
            //init _ratesHistoryArray
            _ratesHistoryArray = [[RatesHistoryArray alloc] initWithDate:date];
        }
        
        //2) get RateNamesArray for our date
        RateNamesArray *currentRateNamesArray = [strongSelf rateNamesArrayByDate:date];
        if (!currentRateNamesArray) {
            [RatesHistoryArray addDateToRatesHistoryArray:_ratesHistoryArray date:date];
            currentRateNamesArray = [strongSelf rateNamesArrayByDate:date];
        }
        
        //3) replace Rate object inside the array
        RateNamesArray *newArray = [strongSelf replaceRateObjectIndexFromArray:currentRateNamesArray withObject:rateObject];
        
        //4) replaceHistoryData
        if (newArray) {
            [strongSelf replaceHistoryArrayObjectByDate:date withArray:newArray];

        }
    });
    dispatch_semaphore_signal(dataReloadSemaphore);
}

-(RateNamesArray* _Nullable)rateNamesArrayByDate:(NSString* _Nonnull)date {
    RateNamesArray *result = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.dateOfRequest = %@", date];
    NSArray *results = [_ratesHistoryArray filteredArrayUsingPredicate:predicate];
    if ([results count] > 0)
        result = (RateNamesArray*)[[results objectAtIndex:0] ratesArray];
    return result;
}

-(RateNamesArray*)replaceRateObjectIndexFromArray:(RateNamesArray*)rateNamesArray
                               withObject:(RateObject*)newRateObject {
    
    NSMutableArray *arr = nil;
    
    for (long i=0; i<[rateNamesArray count]; i++) {
        RateObject *oldRateObject = [rateNamesArray objectAtIndex:i];
        if (
            ([oldRateObject.nameFirst isEqualToString:newRateObject.nameFirst])
            &&
            ([oldRateObject.nameSecond isEqualToString:newRateObject.nameSecond])
        ){
            arr = [[NSMutableArray alloc] initWithArray:rateNamesArray];
            [arr replaceObjectAtIndex:i withObject:newRateObject];
            break;
        }
    }
    return (RateNamesArray*)arr;
}

-(void)replaceHistoryArrayObjectByDate:(NSString*)date withArray:(RateNamesArray*)array {
    for (long i=0; i<[_ratesHistoryArray count]; i++) {
        RatesByDate *rbd = [_ratesHistoryArray objectAtIndex:i];
        
        if ([rbd.dateOfRequest isEqualToString:date]) {
            rbd.ratesArray = array;
            NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:_ratesHistoryArray];
            [arr replaceObjectAtIndex:i withObject:rbd];
            _ratesHistoryArray = (RatesHistoryArray*)arr;
            break;
        }
    }
}

-(void)loadPrevDayData {
    if ([_netLoader noInternetConnection])
        return;
    
    NSString *prevDateInString = [self recievePrevDate];
    
    //http://api.fixer.io/2016-08-29?base=RUB&symbols=USD,EUR
    NSString *shablon = @"%@/%@?base=%@&symbols=%@,%@";
    
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, prevDateInString, @"USD",@"RUB",@"EUR"]];
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, prevDateInString, @"RUB",@"USD",@"EUR"]];
    [_netLoader loadDataFromUrl:[NSString stringWithFormat:shablon, apiUrl, prevDateInString, @"EUR",@"USD",@"RUB"]];
}

-(NSString* _Nonnull)recievePrevDate {
    NSString *currentDate = _latestDateInString; //Format of date 2016-08-30
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSDate *curDate = [dateFormat dateFromString:currentDate];
    NSDate *beforeDate = [NSDate dateWithTimeInterval:-86400 sinceDate:curDate];

    return [dateFormat stringFromDate:beforeDate]; //Format of date 2016-08-29
}

-(RateDescription* _Nonnull)receiveTextDescriptionForRatings:(RateObject* _Nonnull)ro {
    
    RateDescription *result = [[RateDescription alloc] init];
    //1) latest RateObject = ro
    
    //2) receive Previous RateObject
    NSString *prevDateInString = [self recievePrevDate];
    RateNamesArray *ra = [self rateNamesArrayByDate:prevDateInString];
    if ((ra) && ([ra count] > 0)) {
        RateObject *oldestRateObject = nil;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.nameFirst = %@ && SELF.nameSecond = %@", ro.nameFirst, ro.nameSecond];
        
        NSArray *results = [ra filteredArrayUsingPredicate:predicate];
        if ([results count] > 0)
            oldestRateObject = (RateObject*)[results objectAtIndex:0];

        if (oldestRateObject) {
            //calculate if we have Plus or Minus rates
            NSNumber *oldRate = oldestRateObject.rate;
            NSNumber *newRate = ro.rate;
            
            double oldr = [oldRate doubleValue];
            double nldr = [newRate doubleValue];
            
            
            enum RateState rateState = (nldr > oldr)?increase:(nldr < oldr)?decrease:notchanged;
            //65.273
            //64.879
            double percent;
            switch (rateState) {
                case increase:
                    percent = (nldr / (oldr/100)) - 100;
                    break;
                    
                case decrease:
                    percent = 100 - (nldr / (oldr/100));
                    break;

                default:
                    percent = 0;
                    break;
            }
            
            result.rateState = rateState;
            
            
            
            //Со вчерашнего дня доллар\nвырос на 1 процент
            
            NSString *fromYesterday = NSLocalizedString(@"From yesterday", nil);
            NSString *rateOf = NSLocalizedString(@"rate of", nil);
            NSString *notChanged = NSLocalizedString(@"not changed", nil);
            NSString *increaseString = NSLocalizedString(@"increase", nil);
            NSString *decreaseString = NSLocalizedString(@"decrease", nil);
            NSString *lessThen = NSLocalizedString(@"less then", nil);
            NSString *forStr = NSLocalizedString(@"for", nil);
            
            NSString *incDec = (rateState == increase)?increaseString:decreaseString;
            
            if (percent == 0) {
                result.textDescription = [NSString stringWithFormat:@"%@ %@\n%@ %@",
                                          fromYesterday,
                                          rateOf,
                                          ro.localizedFirstName,
                                          notChanged];
            } else if (percent < 1) {
                
                
                result.textDescription = [NSString stringWithFormat:@"%@ %@ %@\n%@ 1 %@",
                                          fromYesterday,
                                          ro.localizedFirstName,
                                          incDec,
                                          lessThen,
                                          NSLocalizedString(@"percent", @"Less Then One percent")];
            } else {
                result.textDescription = [NSString stringWithFormat:@"%@ %@\n%@ %@ %.0f %@",
                                          fromYesterday,
                                          ro.localizedFirstName,
                                          incDec,
                                          forStr,
                                          percent,
                                          [self getLocalizedNumEnds:percent]];
            }
        }
    }
    //TODO else
    return result;
}

-(NSString*)getLocalizedNumEnds:(double)percent {
    NSString *result;
    BOOL locIsRu;
    //TODO move arrays to localizations
    NSArray *endRusArray = @[@"процент", @"процента", @"процентов"];
    NSArray *endEngArray = @[@"percent", @"percents"];
    
    NSString *ident = [[NSLocale currentLocale] localeIdentifier]
    ;
    locIsRu = [[ident substringToIndex:2] isEqualToString:@"ru"];
    if (locIsRu) {
        long number = lround(percent) % 100;
        if ((number >= 11) && (number <= 19)) {
            result = [endRusArray objectAtIndex:2];
        } else {
            long i = lround(percent) % 10;
            switch (i) {
                case 1:
                    result = [endRusArray objectAtIndex:0];
                    break;
                case 2:
                case 3:
                case 4:
                    result = [endRusArray objectAtIndex:1];
                    break;
                default:
                    result = [endRusArray objectAtIndex:2];
                    break;
            }
        }
    } else { //if (locIsRu) {
        //default Base English
        switch (lround(percent)) {
            case 1:
                result = [endEngArray objectAtIndex:0];
                break;
                
            default:
                result = [endEngArray objectAtIndex:1];
                break;
        }
    }
    return result;
}

-(void)showAlert:(NSError* _Nonnull)error {
    [_netLoader showAlert:error];
}

@end
