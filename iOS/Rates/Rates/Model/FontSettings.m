//
//  FontSettings.m
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "FontSettings.h"

static FontSettings *instanceFontsSharedSettings = nil;

@implementation FontSettings

+ (FontSettings *)  sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instanceFontsSharedSettings = [[FontSettings alloc] init];
    });
    return instanceFontsSharedSettings;
}

+ (UIFont *)fontFromRatesFont:(RatesFont)font size:(CGFloat)fontSize {
    UIFont *returnedFont = nil;
    switch (font) {
        case RatesFontLatoBlack:
            returnedFont = [UIFont fontWithName:@"Lato-Black" size:fontSize];
            break;
        case RatesFontLatoBold:
            returnedFont = [UIFont fontWithName:@"Lato-Bold" size:fontSize];
            break;
        case RatesFontLatoMediumItalic:
            returnedFont = [UIFont fontWithName:@"Lato-MediumItalic" size:fontSize];
            break;
        case RatesFontLatoRegular:
            returnedFont = [UIFont fontWithName:@"Lato-Regular" size:fontSize];
            break;
        default:
            break;
    }
    return returnedFont;
}


@end
