//
//  FontSettings.h
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RatesFont) {
    RatesFontLatoBlack,
    RatesFontLatoBold,
    RatesFontLatoMediumItalic,
    RatesFontLatoRegular
};

@interface FontSettings : NSObject

+ (UIFont *)fontFromRatesFont:(RatesFont)font size:(CGFloat)fontSize;

@end
