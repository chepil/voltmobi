//
//  RateObject.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RateObject : NSObject

@property (nonatomic, strong) NSString *nameFirst;
@property (nonatomic, strong) NSString *nameSecond;
@property (atomic, strong) NSNumber *rate;

@property (nonatomic, readonly, getter=getFullStringName) NSString *fullStringName;

@property (nonatomic, readonly, getter=getLocalizedFirstName) NSString *localizedFirstName;

-(id)initWithNameFirst:(NSString*)nameFirst
            nameSecond:(NSString*)nameSecond
                  rate:(NSNumber*)rate;

- (NSString *)description;

@end
