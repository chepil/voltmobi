//
//  UILabel+ratestyle.m
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "UILabel+ratestyle.h"

@implementation UILabel (ratestyle)

- (void) setRatesStyle:(RatesFont)font
                  size:(CGFloat)fontSize
        hexColorString:(NSString*)hexColorString
               spacing:(CGFloat)spacing
               lineHeight:(CGFloat)lineHeight
               opacityInPercent:(unsigned int)opacityInPercent
               paragraphSpacing:(CGFloat)paragraphSpacing
                  text:(NSString*)text {
    
    [self setNumberOfLines:2];
    
    UIFont *attrfont = [FontSettings fontFromRatesFont:font size:fontSize];
    
    UIColor *textColor = [self colorFromHexString:hexColorString alpha:opacityInPercent];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = spacing;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.minimumLineHeight = lineHeight;
    paragraphStyle.maximumLineHeight = lineHeight;
    paragraphStyle.paragraphSpacing = paragraphSpacing;
    
    NSDictionary *attributes = @{NSKernAttributeName:@(spacing),
                                 NSFontAttributeName:attrfont,
                                 NSParagraphStyleAttributeName: paragraphStyle,
                                 NSForegroundColorAttributeName:textColor};
    
    
    NSAttributedString *attrString = [[NSAttributedString alloc]
                                      initWithString:text
                                      attributes:attributes];
    
    [self setAttributedText:attrString];
    
}

//hexString in format #ddee55, dd3355
//inAlpha in format 70 (where 70%)
-(UIColor *)colorFromHexString:(NSString *)hexString alpha:(unsigned int)inAlpha {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 16) & 0xFF)/255.0f;
    float green = ((baseValue >> 8) & 0xFF)/255.0f;
    float blue = ((baseValue >> 0) & 0xFF)/255.0f;
    float alpha = inAlpha / 100.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


@end
