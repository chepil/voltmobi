//
//  RateDescription.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RateDescription : NSObject
@property (nonatomic, strong) NSString *textDescription;
@property (nonatomic) enum RateState rateState;
@end
