//
//  RatesHistoryArray.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "RatesHistoryArray.h"

@implementation RatesHistoryArray

-(id)initWithDate:(NSString*)date {
    if (self = [super init]) {
        
        RatesByDate *rbd = [[RatesByDate alloc] init];
        
        rbd.dateOfRequest = date;
        rbd.ratesArray = [[RateNamesArray alloc] init];
        
        self = (RatesHistoryArray*)@[rbd];
    }
    return self;
}

+(void)addDateToRatesHistoryArray:(RatesHistoryArray* _Nonnull)array date:(NSString* _Nonnull)date; {
    if (array) {
        RatesByDate *rbd = [[RatesByDate alloc] init];
        rbd.dateOfRequest = date;
        rbd.ratesArray = [[RateNamesArray alloc] init];
        [array addObject:rbd];
    }
}


@end
