//
//  RateObject.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "RateObject.h"

@implementation RateObject

-(id)initWithNameFirst:(NSString*)nameFirst
            nameSecond:(NSString*)nameSecond
                  rate:(NSNumber*)rate {
    if (self = [super init]) {
        _nameFirst = nameFirst;
        _nameSecond = nameSecond;
        _rate = rate;
    }
    return self;
}

-(NSString*)getFullStringName {
    return [NSString stringWithFormat:@"%@ → %@", _nameFirst, _nameSecond];
}

-(NSString*)getLocalizedFirstName {
    NSString *locFullName = NSLocalizedString(_nameFirst, @"Localized Full Name");
    return locFullName;
}

- (NSString *)description {
    NSString *descriptionString = [NSString stringWithFormat:@"[ nameFirst: %@, nameSecond: %@, rate: %@ ]\n",
                                   _nameFirst,
                                   _nameSecond,
                                   _rate];
    return descriptionString;
}




@end
