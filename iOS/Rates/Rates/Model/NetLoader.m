//
//  NetLoader.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "NetLoader.h"

@interface NetLoader() <NSURLSessionDelegate, NSURLSessionTaskDelegate>
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) _Nullable id json;
@end

@implementation NetLoader

@synthesize delegate;

-(id)init {
    if (self = [super init]) {

        //disable cache for critical network data
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        _session = [NSURLSession sessionWithConfiguration:configuration];
        
        _noInternetConnection = FALSE;
    }
    return self;
}

-(void)loadDataFromUrl:(nonnull NSString*)stringUrl {
    
    if (_noInternetConnection)
        return;
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSURLSessionDataTask *dataTask = [_session dataTaskWithURL:url
                                             completionHandler:^(NSData *data,
                                                                 NSURLResponse *response,
                                                                 NSError *error)
                                      {
                                          if (!error) {
                                              _json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                              if (_json)
                                                  if (self.delegate && [self.delegate respondsToSelector:@selector(didLoadJson:)]) {
                                                      [self.delegate didLoadJson:_json];
                                                  }
                                              
                                              [[NSNotificationCenter defaultCenter] postNotificationName:onlineNotification
                                                                                                  object:nil userInfo:nil];
                                          } else {
                                              
                                              if (!_noInternetConnection) {
                                                  if ([[Singleton sharedInstance] mainViewController]) {
                                                      dispatch_async(dispatch_get_main_queue(), ^(){
                                                          [[[[Singleton sharedInstance] mainViewController] actNetworkWaiting] stopAnimating];
                                                      });
                                                  }
                                                  [self showAlert:error];
                                              }
                                              _noInternetConnection = TRUE;
                                              
                                              [[NSNotificationCenter defaultCenter] postNotificationName:offlineNotification
                                                                                                  object:nil userInfo:nil];
                                              
                                          }
                                      }];
    [dataTask resume];
}

-(void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
#ifdef DEBUG
    NSLog(@"%@", error);
#endif
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCompleteWithError:)]) {
        [self.delegate didCompleteWithError:error];
    }
}

-(void)showAlert:(NSError* _Nonnull)error {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Error", @"Error dialog title")
                                          message:error.localizedDescription
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Retry", @"Retry action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       _noInternetConnection = FALSE;
                                       [[Singleton sharedInstance] reloadData];
                                   });
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       _noInternetConnection = TRUE;
                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"offline" object:nil userInfo:nil];
                                   });
                               }];
    
    [alertController addAction:okAction];
    
    if ([[[Singleton sharedInstance] ratesHistoryArray] count] > 0) {
        [alertController addAction:cancelAction];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[Singleton sharedInstance] mainViewController] presentViewController:alertController animated:YES completion:nil];
    });
}


@end
