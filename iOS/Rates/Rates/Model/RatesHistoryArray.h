//
//  RatesHistoryArray.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RatesHistoryArray : NSMutableArray

-(id _Nonnull)initWithDate:(NSString* _Nonnull)date;
+(void)addDateToRatesHistoryArray:(RatesHistoryArray* _Nonnull)array date:(NSString* _Nonnull)date;


@end
