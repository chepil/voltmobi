//
//  JsonDataParser.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "JsonDataParser.h"

@implementation JsonDataParser

dispatch_semaphore_t checkLastDateSemaphore;

-(instancetype)init {
    if (self = [super init]) {
        checkLastDateSemaphore = dispatch_semaphore_create(1);
    }
    return self;
}

#pragma mark -
#pragma mark LoadData

-(void)loadData:(id)jsonData {
    RateObject *ro;
    NSString *base = ([jsonData objectForKey:@"base"])?[jsonData objectForKey:@"base"]:nil;
    NSString *date = ([jsonData objectForKey:@"date"])?[jsonData objectForKey:@"date"]:nil;
    
    if (date) {
        dispatch_semaphore_wait(checkLastDateSemaphore, DISPATCH_TIME_FOREVER);
        dispatch_sync(dispatch_get_main_queue(), ^(){
            if ([[Singleton sharedInstance] needLatestDateUpdate]) {
                [[Singleton sharedInstance] setNeedLatestDateUpdate:FALSE];
                //нужно установить эту переменную и запустить загрузку предыдущего дня
                [[Singleton sharedInstance] setLatestDateInString:date];
                [[Singleton sharedInstance] loadPrevDayData];
            }
        });
        dispatch_semaphore_signal(checkLastDateSemaphore);
    }
    
    NSDictionary *rates = ([jsonData objectForKey:@"rates"])?[jsonData objectForKey:@"rates"]:nil;
    if ( base && date && rates ){
        for ( NSString *key in rates) {
            NSNumber *value = [rates objectForKey:key];
            
            if (key && value) {
                ro = [[RateObject alloc] initWithNameFirst:base
                                                            nameSecond:key
                                                                  rate:value];
                [[Singleton sharedInstance] writeInfoByDate:date andRateNamesArray:ro];
                [[Singleton sharedInstance] incReceiveDataCounter];
            }
        }
    } else {
        //TODO add reload network data
        NSLog(@"something wrong with json results: %@", jsonData);
    }    
}

@end
