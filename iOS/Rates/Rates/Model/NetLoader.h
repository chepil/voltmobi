//
//  NetLoader.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetLoader;

@protocol NetLoaderDelegate <NSObject>

@optional
-(void)didLoadJson:(_Nullable id)jsonData;
-(void)didCompleteWithError:(NSError* _Nullable)error;

@end

@interface NetLoader : NSObject

@property _Nullable id <NetLoaderDelegate> delegate;
@property (atomic) BOOL noInternetConnection;

-(void)loadDataFromUrl:(nonnull NSString *)stringUrl;

-(void)showAlert:(NSError* _Nonnull)error;

@end
