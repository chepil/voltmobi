//
//  Singleton.h
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetLoader.h"
#import "MainViewController.h"

#define firstObjectRecievedNotification @"firstObjectRecieved"
#define offlineNotification @"offline"
#define onlineNotification @"online"

enum RateState {
    increase = 0,
    decrease,
    notchanged
};

enum ViewStyle {
    compactView = 0,
    normalView
};

@interface Singleton : NSObject <NetLoaderDelegate>

@property (nonatomic, strong) NetLoader * _Nonnull netLoader;
@property (atomic, strong) RatesHistoryArray * _Nonnull ratesHistoryArray;
@property (atomic, strong) NSString * _Nonnull latestDateInString;
@property (atomic) BOOL needLatestDateUpdate;


@property (atomic) long receiveDataCounter;

@property (nonatomic, strong) MainViewController * _Nonnull mainViewController;

@property (nonatomic) long lastSelectedIndex;

+(id _Nonnull)sharedInstance;

-(void)writeInfoByDate:(NSString* _Nonnull)date andRateNamesArray:(RateObject* _Nonnull)rateObject;

-(RateNamesArray* _Nullable)rateNamesArrayByDate:(NSString* _Nonnull)date;

-(void)loadPrevDayData;
-(long)incReceiveDataCounter;
-(void)reloadData;
-(RateDescription* _Nonnull)receiveTextDescriptionForRatings:(RateObject* _Nonnull)ro;

-(void)showAlert:(NSError* _Nonnull)error;
-(void)forceUpdate;

@end
