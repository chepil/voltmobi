//
//  RateNamesArray.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "RateNamesArray.h"

@implementation RateNamesArray

-(id)init {
    if (self = [super init]) {
        
        RateObject *roUSDRUB = [[RateObject alloc] initWithNameFirst:@"USD"
                                                          nameSecond:@"RUB"
                                                                rate:@0];
        
        RateObject *roUSDEUR = [[RateObject alloc] initWithNameFirst:@"USD"
                                                          nameSecond:@"EUR"
                                                                rate:@0];
        
        RateObject *roEURRUB = [[RateObject alloc] initWithNameFirst:@"EUR"
                                                          nameSecond:@"RUB"
                                                                rate:@0];
        
        RateObject *roEURUSD = [[RateObject alloc] initWithNameFirst:@"EUR"
                                                          nameSecond:@"USD"
                                                                rate:@0];
        
        RateObject *roRUBUSD = [[RateObject alloc] initWithNameFirst:@"RUB"
                                                          nameSecond:@"USD"
                                                                rate:@0];
        
        RateObject *roRUBEUR = [[RateObject alloc] initWithNameFirst:@"RUB"
                                                          nameSecond:@"EUR"
                                                                rate:@0];

        self = (RateNamesArray*)@[roUSDRUB, roUSDEUR, roEURRUB, roEURUSD, roRUBUSD, roRUBEUR];
    }
    return self;
}

@end
