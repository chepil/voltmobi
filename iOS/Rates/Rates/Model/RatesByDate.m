//
//  RatesHistory.m
//  Rates
//
//  Created by Denis Chepil on 31.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "RatesByDate.h"

@implementation RatesByDate

-(NSString*)description {
    NSString *descriptionString =[NSString stringWithFormat:@"date:%@, array:%@",
                                  _dateOfRequest,
                                  _ratesArray];
    return descriptionString;
}
@end
