//
//  RateViewController.h
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateViewController : UIViewController

//-(void)setupLabels;
-(void)setupLabelsWithName:(NSString*)rateName
                      rate:(NSString*)rate
                      text:(NSString*)text
                 rateState:(enum RateState)rateState;

-(void)setupCompactView;
-(void)setupNormalView;
-(void)setupRatings:(RateObject*)rateObject;


@end
