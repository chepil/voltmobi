//
//  RateViewController.m
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "RateViewController.h"

#define ratesNameLblColor @"#3F4753"
#define rateLblColor @"#3F4753"
#define textDescriptionDecreaseColor @"#D0021B"
#define textDescriptionIncreaseColor @"#7ED321"

@interface RateViewController ()
@property (weak, nonatomic) IBOutlet UILabel *ratesNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *rateLbl;
@property (weak, nonatomic) IBOutlet UILabel *textDescriptionOfRateChangesLbl;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLayoutForRatesNameLbl;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLayoutForRateLbl;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLayoutForTextDescriptionOfRateChangesLbl;
@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupLabelsWithName:(NSString*)rateName
                      rate:(NSString*)rate
                      text:(NSString*)text
                 rateState:(enum RateState)rateState
{
    [_ratesNameLbl setRatesStyle:RatesFontLatoBold
                            size:17.0f
                  hexColorString:ratesNameLblColor
                         spacing:1.0f
                      lineHeight:21.0f
                opacityInPercent:70
                paragraphSpacing:0
                            text:rateName];
    
    [_rateLbl setRatesStyle:RatesFontLatoRegular
                       size:80.0f
             hexColorString:rateLblColor
                    spacing:-4.0f
                 lineHeight:96.0f
           opacityInPercent:100
           paragraphSpacing:0
                       text:rate];
    
    NSString *hexColorForTextString = (rateState == decrease)?textDescriptionDecreaseColor:textDescriptionIncreaseColor;
    
    [_textDescriptionOfRateChangesLbl setRatesStyle:RatesFontLatoMediumItalic
                                               size:17.0f
                                     hexColorString:hexColorForTextString
                                            spacing:0.0f
                                         lineHeight:41.0f
                                   opacityInPercent:100
                                   paragraphSpacing:-21
                                               text:text
    ];
}

-(void)setupCompactView {
    NSArray *sizes = [self setupConstantsForStyle:compactView];
    
    _topLayoutForRatesNameLbl.constant = [sizes[0] floatValue];
    _topLayoutForRateLbl.constant = [sizes[1] floatValue];
    _topLayoutForTextDescriptionOfRateChangesLbl.constant = [sizes[2] floatValue];
}

-(void)setupNormalView {
    NSArray *sizes = [self setupConstantsForStyle:normalView];

    _topLayoutForRatesNameLbl.constant = [sizes[0] floatValue];
    _topLayoutForRateLbl.constant = [sizes[1] floatValue];
    _topLayoutForTextDescriptionOfRateChangesLbl.constant = [sizes[2] floatValue];
    [self.view layoutSubviews];
}

-(void)setupRatings:(RateObject*)ro {
    NSDecimalNumber * dn = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%0.03f",[ro.rate doubleValue]]];
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setMinimumFractionDigits:3];
    [nf setMaximumFractionDigits:3];
    [nf setMinimumIntegerDigits:1];
    NSString *stringRate  = [nf stringFromNumber:dn];

    RateDescription *rd = [[Singleton sharedInstance] receiveTextDescriptionForRatings:ro];
    
    [self setupLabelsWithName:ro.fullStringName
                         rate:stringRate
                         text:rd.textDescription
                    rateState:rd.rateState];
}

-(NSArray*)setupConstantsForStyle:(enum ViewStyle)viewStyle {
    NSArray *result;
    
    CGFloat h = [UIScreen mainScreen].bounds.size.height;
    
    //FEATURE
    //Это сделано на будущее, так как, возомжно, захочется иметь различные константы для всех разрешений экрана. Сейчас, они одинаковы.
    
    switch ((long)h) {
        case 480:
            result = (viewStyle == compactView)?@[@0,@12,@88]:@[@0,@18,@98];
            break;
        case 568:
            result = (viewStyle == compactView)?@[@0,@12,@88]:@[@0,@18,@98];
            break;
        case 667:
            result = (viewStyle == compactView)?@[@0,@12,@88]:@[@0,@18,@98];
            break;
        case 736:
         default:
            result = (viewStyle == compactView)?@[@0,@12,@88]:@[@0,@18,@98];
            break;
    }
    
    return result;
}

@end
