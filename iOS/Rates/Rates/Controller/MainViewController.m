//
//  MainViewController.m
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "MainViewController.h"
#import "RateViewController.h"
#import "SettingsViewController.h"

#define hidden_settings_bottom_constraint -264.0f;
#define show_settings_bottom_constraint 0.0f;


@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UILabel *updateStatusLbl;
@property (weak, nonatomic) IBOutlet UIButton *settingsBtn;
@property (nonatomic, strong) RateViewController *rateViewController;
@property (nonatomic, strong) SettingsViewController *settingsViewController;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLayoutForRate;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomLayoutForSettings;


@property (nonatomic, strong) NSTimer *waitForLoadTimer;

@property (weak, nonatomic) IBOutlet UIButton *offlineButton;


@end

@implementation MainViewController

@synthesize rateViewController = _rateViewController;
@synthesize settingsViewController = _settingsViewController;

#pragma mark -
#pragma mark Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[Singleton sharedInstance] setMainViewController:self];
    
    _waitForLoadTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(noDataReceived:) userInfo:nil repeats:NO];
    
    [_actNetworkWaiting startAnimating];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showRateOnTheScreen:)
                                                 name:firstObjectRecievedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showOffline)
                                                 name:offlineNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showOnline)
                                                 name:onlineNotification
                                               object:nil];

    
    _topLayoutForRate.constant = [[self setupConstants][0] floatValue]; //start top constant
    _bottomLayoutForSettings.constant = hidden_settings_bottom_constraint;
    
    [_updateStatusLbl setAlpha:0.0f];
    [_settingsBtn setAlpha:0.0f];
    
    [_offlineButton setTitle:NSLocalizedString(@"Offline", nil) forState:UIControlStateNormal];
    
    [self setupLabels];
    
}



-(void)setupLabels {
    NSDate *lastUpd = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *lastUpdate = [dateFormat stringFromDate:lastUpd];
    
    NSString *updateAt = NSLocalizedString(@"UPDATED", nil);
    NSString *text = [NSString stringWithFormat:@"%@ %@",updateAt, lastUpdate];
    
    [_updateStatusLbl setRatesStyle:RatesFontLatoBlack
                               size:11.0f
                     hexColorString:@"#3F4753"
                            spacing:1.2f
                         lineHeight:13.5f
                   opacityInPercent:40
                   paragraphSpacing:0
                               text:text];
    
    
}

#pragma mark -
#pragma mark Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"rateSegue"]) {
        _rateViewController = segue.destinationViewController;
        [_rateViewController.view setAlpha:0.0f];
    }
    if ([segue.identifier isEqualToString:@"settingsSegue"]) {
        _settingsViewController = segue.destinationViewController;
        [_settingsViewController setMainViewController:self];
        [_settingsViewController.view setAlpha:0.0f];
    }
}

- (IBAction)settingsButtonPress:(id)sender {
    [_settingsViewController reloadData];
    [self showSettings];
}


#pragma mark - 
#pragma mark Animations

-(void)showRateAnimationFromBottomToTop {
    [_rateViewController setupCompactView];
    [UIView animateWithDuration:1.0f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(){
                         _topLayoutForRate.constant = [[self setupConstants][1] floatValue]; //default top constant
                         
                         [_rateViewController.view setAlpha:1.0f];
                         
                         [self.view layoutSubviews];
                         
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:1.5f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^(){
                                              [_updateStatusLbl setAlpha:1.0f];
                                              [_rateViewController setupNormalView];
                                              [self.view layoutSubviews];
                                          }
                                          completion:^(BOOL finished) {
                                              [_settingsBtn setAlpha:1.0f];
                                              
                                          }];
                     }];
}

-(void)showSettings {
    
    [[Singleton sharedInstance] reloadData];
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(){
                         [_updateStatusLbl setAlpha:0.0f];
                         [_settingsBtn setAlpha:0.0f];
                     }
                     completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:1.0f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(){
                         _topLayoutForRate.constant = [[self setupConstants][2] floatValue]; //upper top constant
                         _bottomLayoutForSettings.constant = show_settings_bottom_constraint;
                         [_rateViewController setupCompactView];
                         [_settingsViewController.view setAlpha:1.0f];
                         
                         [self.view layoutSubviews];
                         [_rateViewController.view layoutSubviews];
                     } completion:^(BOOL finished) {
                     }];
}

-(void)hideSettings {
    [UIView animateWithDuration:1.0f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(){
                         _topLayoutForRate.constant = [[self setupConstants][1] floatValue]; //default top constant
                         _bottomLayoutForSettings.constant = hidden_settings_bottom_constraint;
                         [_rateViewController setupNormalView];
                         [_settingsViewController.view setAlpha:0.0f];
                         
                         [self.view layoutSubviews];
                         [_rateViewController.view layoutSubviews];
                         
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^(){
                                              [_updateStatusLbl setAlpha:1.0f];
                                          }
                                          completion:^(BOOL finished) {
                                              [_settingsBtn setAlpha:1.0f];
                                          }];
                     }];
}


#pragma mark -
#pragma mark Other

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:firstObjectRecievedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:offlineNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:onlineNotification object:nil];
    [super viewDidDisappear:animated];
}

-(void)noDataReceived:(NSTimer*)timer {
    [_actNetworkWaiting stopAnimating];
    
    [_waitForLoadTimer invalidate];
    _waitForLoadTimer = nil;
    //TODO alert with error of loading
    NSString *noAnswerFromServer = NSLocalizedString(@"No answer from Server", nil);
    
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:noAnswerFromServer};
    NSError *error = [[NSError alloc] initWithDomain:@"customError" code:666 userInfo:userInfo];
    [[Singleton sharedInstance] showAlert:error];
}


#pragma mark -
#pragma mark Show Rates

-(void)showRateOnTheScreen:(NSNotification*)note {
    [_waitForLoadTimer invalidate];
    _waitForLoadTimer = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^(){
        [_actNetworkWaiting stopAnimating];
        
        [self showRateByIndex:[[Singleton sharedInstance] lastSelectedIndex]];
        if (_settingsViewController.view.alpha == 0.0f)
            [self showRateAnimationFromBottomToTop];
    });
}

-(void)showRateByIndex:(long)index {
    RateNamesArray *rateNamesArray = [[Singleton sharedInstance] rateNamesArrayByDate:[[Singleton sharedInstance] latestDateInString]];
    if (rateNamesArray) {
        RateObject *ro = [rateNamesArray objectAtIndex:index];
        [_rateViewController setupRatings:ro];
    } else {
        NSLog(@"no data found");
    }
}

-(NSArray*)setupConstants {
    NSArray *result;
    
    CGFloat h = [UIScreen mainScreen].bounds.size.height;
    
    switch ((long)h) {
        case 480:
            result = @[@160,@80,@16];
            break;
        case 568:
            result = @[@204,@124,@60];
            break;
        case 667:
            result = @[@254,@174,@110];
            break;
        case 736:
        default:
            result = @[@288,@208,@144];
            break;
    }
    
    return result;
}

#pragma mark -
#pragma mark Offline logic
- (IBAction)offlineButtonPress:(id)sender {
    [[Singleton sharedInstance] forceUpdate];
}

-(void)showOffline {
    dispatch_async(dispatch_get_main_queue(), ^(){
        [_offlineButton setHidden:FALSE];
    });
    
}

-(void)showOnline {
    dispatch_async(dispatch_get_main_queue(), ^(){
        [_offlineButton setHidden:TRUE];
        [self setupLabels];
    });
}



@end
