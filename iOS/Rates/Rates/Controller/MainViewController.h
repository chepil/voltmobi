//
//  MainViewController.h
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actNetworkWaiting;

-(void)hideSettings;
-(void)showRateByIndex:(long)index;


@end

