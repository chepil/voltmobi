//
//  SettingsViewController.h
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) MainViewController *mainViewController;
-(void)reloadData;
@end
