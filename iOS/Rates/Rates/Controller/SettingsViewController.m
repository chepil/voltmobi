//
//  SettingsViewController.m
//  Rates
//
//  Created by Denis Chepil on 30.08.16.
//  Copyright © 2016 Denis Chepil. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"

#define colorForCellText @"#FFFFFF"

@interface SettingsViewController ()
@property (nonatomic, weak) IBOutlet UITableView *tblView;
@property (nonatomic, strong) RateNamesArray *rateNamesArray;
@property (nonatomic, strong) NSIndexPath *selectedIndex;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _selectedIndex = [self.tblView indexPathForSelectedRow];
    if (!_selectedIndex) {
        _selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    }
        
    _tblView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)reloadData {
    _rateNamesArray = [[Singleton sharedInstance] rateNamesArrayByDate:[[Singleton sharedInstance] latestDateInString]];
    [self.tblView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (_rateNamesArray)?[_rateNamesArray count]:0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingsTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"cell"];
    
    RateObject *ro = [_rateNamesArray objectAtIndex:indexPath.row];
     
    if ((_selectedIndex) && (_selectedIndex.row == indexPath.row)) {
        [cell.rateLbl setRatesStyle:RatesFontLatoBlack
                               size:14.0f
                     hexColorString:colorForCellText
                            spacing:1.0f
                         lineHeight:0.0f
                   opacityInPercent:100
                   paragraphSpacing:0
                               text:ro.fullStringName];
    } else {
        [cell.rateLbl setRatesStyle:RatesFontLatoRegular
                               size:14.0f
                     hexColorString:colorForCellText
                            spacing:1.0f
                         lineHeight:0.0f
                   opacityInPercent:70
                   paragraphSpacing:0
                               text:ro.fullStringName];
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self selectRow:indexPath];
}

-(void)selectRow:(NSIndexPath *)indexPath {
    NSIndexPath *oldIndexPath = [_selectedIndex copy];
    _selectedIndex = indexPath;
    if (oldIndexPath) {
        [self.tblView reloadRowsAtIndexPaths:@[oldIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    [UIView animateWithDuration:0.6 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^(){
        [self.tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } completion:^(BOOL finished){
        [_mainViewController hideSettings];
        [_mainViewController showRateByIndex:indexPath.row];
    }];
    [[Singleton sharedInstance] setLastSelectedIndex:indexPath.row];
}

@end
